FROM openjdk:8
ADD target/gs-actuator-service-0.1.0.jar dockerchess.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "dockerchess.jar"]