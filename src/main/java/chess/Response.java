//Response class is used to generate the response message to the incoming HTTP requests from the user
package chess;

import java.util.ArrayList;

public class Response {
    private String responseContent;                                                                                     //general string response
    private String gameId;                                                                                              //game id
    private Integer fromX;                                                                                              //x co-ordinate of start position
    private Integer fromY;                                                                                              //y co-ordinate of start position
    private Integer toX;                                                                                                //x co-ordinate of final position
    private Integer toY;                                                                                                //y co-ordinate of final position

    public Response(String responseContent){
        this.responseContent = responseContent;
    }

    //Constructor setting the values of object
    public Response(String responseContent,String gameId, ArrayList<Integer> listOfSquares ){
        this.responseContent = "Computer Move";
        this.gameId = gameId;
        this.fromX = listOfSquares.get(0);
        this.fromY = listOfSquares.get(1);
        this.toX = listOfSquares.get(2);
        this.toY = listOfSquares.get(3);
    }

    public String getResponse(){
        return responseContent;                                                                                         //Return general String content
    }

    public String getGameId(){
        return gameId;                                                                                                  //Return game id
    }
    public Integer getFromX(){
        return fromX;                                                                                                   //Return x co-ordinate of start position
    }
    public Integer getFromY(){
        return fromY;                                                                                                   //Return y co-ordinate of start position
    }
    public Integer getToX(){                                                                                            //Return x co-ordinate of final position
        return toX;
    }
    public Integer getToY(){                                                                                            //Return y co-ordinate of final position
        return toY;
    }

}
