### CS 441 HW4 - Create a chess game VAP using a unikernel OS and a containerization toolkit
### Submitted By - Joylyn Lewis

The Chess Game VAP is built in Java using the Java Open Chess source code (https://sourceforge.net/projects/javaopenchess/). The JAR file available in the Files tab at this link was downloaded and added into the \lib folder of the SpringBoot_Chess project structure. 
Rather than removing all GUI related code from the Java Open Chess source code, a wrapper class **"JChessWrapper.java"** was created which extracts only the core objects of the chess engine from the GUI and these were integrated into the web service which is built using Spring Boot. The VAP is designed to be played in Human vs Computer mode where a Human player provides his/her moves and in response, the computer's moves will be obtained.
The other classes written for the Chess VAP are as below:  
- **ChessApplication.java** : This class is treated as the 'Main Class' from which the Java Application will run  
- **ChessController.java** : This class handles all incoming HTTP requests from the user and returns an appropriate response  
- **JChessEngine.java** : This class maintains a hashmap where the key is the gameId and value is the game object. This class is used to keep track of the different games that are played when the Spring Boot service is run  
- **Response.java** : This class is used to generate the response message to the incoming HTTP requests from the user  
- **ChessControllerTest.java** : This class contains the Unit Tests written to test the Web Service REST API calls  

Apart from the classes, below are other important files included in the Bitbucket repository:  
- **Capstanfile**: Contains configuration parameters required for creating the OSV image  
- **Dockerfile**: Contains configuration parameters required for creating the Docker image  
- **Screenshots of Build_Tests_Local_Docker Postman Requests.docx** : Contains screenshots of maven build, unit tests and postman REST requests testing the various endpoints both with the web service running on local workstation and on running the Docker image  
- **Screenshots of deploying Docker Container to AWS.docx** : Contains screenshots of deploying and running the docker image on AWS EC2 instance. Also, REST requests to test the API using Postman are included in this document.

The main functioanlity designed in this Chess VAP implementation include the following:  
- createGame(gameId, firstMove) : Here, the user can provide the gameId which is any String value (could be 1, 2 etc) and the firstMove is a boolean variable where value true means that the user can play WHITE and value false means that the user can play BLACK.  
- If the user selects to play WHITE, the user can submit a move against the gameId using the move(fromX, fromY, toX, toY) function. Here, fromX, fromY, toX, toY refer to the coordinates of the chess board game.  
- The columns(x co-ordinates 0 to 7) of rows(y co-ordinates 6 and 7) belong to the person playing WHITE.  
- The columns(x co-ordinates 0 to 7) of rows(y co-ordinates 0 and 1) belong to the person playing BLACK.  
- If the user selects to play BLACK, then the computer's first move can be obtained by invoking the function firstMoveByComputer()  
- The user can continue to submit moves via the move(fromX, fromY, toX, toY) function and in response, the computer's moves will be obtained.  
- Different values of gameIds maybe created by the user in order to play multiple games in the Human vs Computer mode during the period when the Spring Boot service is run.  
- The player can quit the game at any time by invoking the function quit()

**REST Endpoints:**

	**Request Type**	**Endpoint**	                                            	**Description**
- GET or POST	    	**<url>/chess**	                                                Displays a Welcome message  
- POST	   				**<url>/chess/game?gameId=1&firstMove=true**	                Create a new game by providing the gameId and flag indicating whether to play WHITE or BLACK  
- GET	        		**<url>/chess/game/{gameId}**                                	Check whether gameId exists or not for the session when the service is run  
- GET	        		**<url>chess/game/{gameId}/getFirstMove**                    	Get first move from Computer when user chooses to play BLACK  
- POST	    		    **<url>chess/game/{gameId}/move?fromX=0&fromY=6&toX=0&toY=4**	Provide move based on Start and End X, Y co-ordinates of the chess board and obtain computer move in response  
- POST	    			**<url>chess/game/quit?gameId=1**                           	Quit the game

The response generated is in JSON format. Sample response of computer's move is as below:
{  
    "gameId": "2",  
	"fromX": 2,  
	"fromY": 6,  
	"toX": 2,  
	"toY": 4,  
	"response": "Computer Move"
}

Maven build command is used to build the application: mvn clean package.
The unit tests are written using REST-assured to test and validate the REST APIs.

For Building the Docker image:

Command to build image: **docker build -f Dockerfile -t dockerchess .**  
Command to run the image: **docker run -p 8080:8080 dockerchess**  

The generated Docker image hs been pushed to the Docker Hub public repository: **https://cloud.docker.com/repository/docker/joylyn133/joylyn_lewis_hw4_dockerimage**  
Account name: **joylyn133**  
Repository name: **joylyn_lewis_hw4_dockerimage**  
Tag name: **hw4**  

Screenshots of the Postman requests to test the API services from running Spring Boot both via local system and Docker image have been documented in the file **'Build_Tests_Local_Docker Postman Requests.docx'**
Screenshots of the Postman requests to test the API services from running Docker image on AWS EC2 instance have been documented in the file **'Screenshots of deploying Docker Container to AWS.docx'**  

Challenges faced:  
- Generating the OSV image for the chess application did not succeed. Tried out the following approaches but in each case, ran into different issues:  
-- Tried running Ubuntu 18.04 by enabling the Windows Subsystem for Linux but ran into KVM installation issue which prevented Capstan from working properly  
-- Tried setting up VM VirtualBox. While a sample 'Hello World' image worked, on trying to create the OSV image for the Chess App via Capstan resulted in error 'The command line is empty'  
-- Tried setting up vmWare by installing Ubuntu and then enabling Intel VT-x and CPU performance counters under the processor settings of the VM. But ran into QEMU/KVM issues  

Overall, although the OSV image could not be generated in order to complete the basic requirements of the homework, Capstanfile is included in this repository and capturing the other basic aspects of the homework properly has been attempted. 



