//JChessWrapper class is the wrapper class which includes the core objects from the Java Open Chess code. This class enables to maintain the chess state without any GUI.
package chess;

import pl.art.lach.mateusz.javaopenchess.core.Chessboard;
import pl.art.lach.mateusz.javaopenchess.core.Game;
import pl.art.lach.mateusz.javaopenchess.core.Square;
import pl.art.lach.mateusz.javaopenchess.core.ai.AI;
import pl.art.lach.mateusz.javaopenchess.core.ai.joc_ai.Level1;
import pl.art.lach.mateusz.javaopenchess.core.moves.Move;
import pl.art.lach.mateusz.javaopenchess.core.pieces.Piece;
import pl.art.lach.mateusz.javaopenchess.core.pieces.implementation.King;
import pl.art.lach.mateusz.javaopenchess.core.players.Player;
import pl.art.lach.mateusz.javaopenchess.core.players.PlayerType;
import pl.art.lach.mateusz.javaopenchess.core.players.implementation.ComputerPlayer;
import pl.art.lach.mateusz.javaopenchess.core.players.implementation.HumanPlayer;
import pl.art.lach.mateusz.javaopenchess.utils.GameTypes;
import pl.art.lach.mateusz.javaopenchess.utils.Settings;
import java.util.ArrayList;
import static pl.art.lach.mateusz.javaopenchess.core.Colors.BLACK;
import static pl.art.lach.mateusz.javaopenchess.core.Colors.WHITE;

public class JChessWrapper {
    //Function to check whether the move invoked by the user can be performed or not. This is based on whether the square is valid,
    // contains a chess piece and whether it is the player's turn
    public boolean cannotInvokeMoveAction(Square sq, Game gameObject) {
        return ((sq == null || sq.piece == null) && gameObject.getChessboard().getActiveSquare() == null)
                || (gameObject.getChessboard().getActiveSquare() == null && sq.piece != null
                && sq.getPiece().getPlayer() != gameObject.getActivePlayer());
    }

    //Function to check whether the move action can be invoked. This is based on whether the start square is valid, the start square contains a chess piece
    //and that the end position square can be played by the chess piece
    private boolean canInvokeMoveAction(Square startSquare, Square endSquare, Game gameObject )
    {
        Square activeSq = startSquare;
        return activeSq != null && activeSq.piece != null && activeSq.getPiece().getAllMoves().contains(endSquare);
    }

    //Function to check whether other pieces on the chess board can move based on all moves that can be played by the pieces on the board
    public boolean otherPiecesCanMove(Game gameObject, King king)
    {
        for (int i = Chessboard.FIRST_SQUARE; i <= Chessboard.LAST_SQUARE; ++i)
        {
            for (int j = Chessboard.FIRST_SQUARE; j <= Chessboard.LAST_SQUARE; ++j)
            {
                Piece piece = gameObject.getChessboard().getSquare(i, j).getPiece();
                if (null != piece && piece.getPlayer() == king.getPlayer()
                        && !piece.getAllMoves().isEmpty())
                {
                    return true;
                }
            }
        }
        return false;
    }

    //Function to check state of the king based on whether the king can play further moves, is checkmate or otherwise
    public KingState getKingState(King king, Game gameObject)
    {
        if (king.getAllMoves().isEmpty())
        {
            if (otherPiecesCanMove(gameObject, king))
            {
                return KingState.FINE;
            }
            else
            {
                if (king.isChecked())
                {
                    return KingState.CHECKMATED;
                }
                else
                {
                    return KingState.STEALMATED;
                }
            }
        }
        return KingState.FINE;
    }

    //Function to actually make th emove on the chess board based on user inputs
    private void invokeMoveAction(Square startSquare, Square endSquare, Game gameObject)
    {
        if (gameObject.getSettings().getGameType() == GameTypes.LOCAL)
        {
            //Here the actual move is initiated
            gameObject.getChessboard().move(startSquare, endSquare);
        }
        gameObject.getChessboard().unselect();

        //Here the nextMove function is called to switch player and maintain state
        gameObject.nextMove();

        //This is to check the state of the king whether fine or checkmate
        King king;
        if (gameObject.getSettings().getPlayerWhite() == gameObject.getActivePlayer())
        {
            king = gameObject.getChessboard().getKingWhite();
        } else
        {
            king = gameObject.getChessboard().getKingBlack();
        }

        //Message for the end game based on whether king is checkmate or game is draw
        switch (getKingState(king, gameObject))
        {
            case CHECKMATED:
                gameObject.endGame(String.format("Checkmate! %s player lose!", king.getPlayer().getColor().toString()));
                break;
            case STEALMATED:
                gameObject.endGame("Stalemate! Draw!");
                break;
            case FINE:
                break;
        }
    }

    //Function to check whether computer can make a move in response to user's move. This is based on the current status of the game, the current player is the Computer
    //and desired AI level is obtained
    public boolean canDoComputerMove(Game gameObject)
    {
        return !gameObject.isIsEndOfGame() && gameObject.getSettings().isGameAgainstComputer()
                && gameObject.getActivePlayer().getPlayerType() == PlayerType.COMPUTER && null != gameObject.getAi();
    }

    //Function to actually make the computer move on the board and maintain the state of the game
    public ArrayList<Integer> doComputerMove(Game gameObject){
        //Obtain the last move made by the human player
        Move lastMove = gameObject.getMoves().getLastMoveFromHistory();
        //Get the move based on the AI level
        Move move = gameObject.getAi().getMove(gameObject, lastMove);
        //Make the actual computer move
        gameObject.getChessboard().move(move.getFrom(), move.getTo());

        ArrayList<Integer> listOfSquares = new ArrayList<Integer>();
        if (null != move.getPromotedPiece())
        {
            move.getTo().setPiece(move.getPromotedPiece());
        }
        //Obtain the x, y co-ordinates of the initial and final positons of the move made by computer
        listOfSquares.add(move.getFrom().getPozX());
        listOfSquares.add(move.getFrom().getPozY());
        listOfSquares.add(move.getTo().getPozX());
        listOfSquares.add(move.getTo().getPozY());
        //Invoke nextMove to switch player and continue maintaining the state of the game
        gameObject.nextMove();
        return listOfSquares;
    }

    //Function invoked when user wants to play a move
    public ArrayList<Integer> move(int fromX, int fromY, int toX, int toY, Game gameObject){
        //Square object is constructed based on the coordinates of the start and end position of the move on chess board
        Square startSquare = gameObject.getChessboard().getSquare(fromX, fromY);
        Square endSquare = gameObject.getChessboard().getSquare(toX, toY);
        Chessboard chessboard = gameObject.getChessboard();

        ArrayList<Integer> listOfSquares = new ArrayList<Integer>();

        //Invalid move is returned as an empty list if the move is illegal
        if(cannotInvokeMoveAction(startSquare, gameObject)) {
            System.out.println("Invalid Move");
            ArrayList<Integer> listForInvalidMoves = new ArrayList<Integer>(4);
            return listForInvalidMoves;
        }

        //If move is legal, then the move is actually made on the chess board
        if (canInvokeMoveAction(startSquare, endSquare, gameObject))
        {
            invokeMoveAction(startSquare, endSquare, gameObject);
        }
        //If computer move is legal, then the move is actually made on the board and the state of the game is maintained
        if (canDoComputerMove(gameObject))
        {
            listOfSquares= doComputerMove(gameObject);
            gameObject.updateFenStateText();
        }

        return listOfSquares;
    }

    //Function to set up new game with paramter firstMove indicating whether the player wants to play WHITE or BLACK
    public Game newGame(boolean firstMove) {
        Game gameObject = new Game();
        Player player1;
        Player player2;

        //Here game is set up as between HUman Player and Computer Player
        if (firstMove == true){
            player1 = new HumanPlayer("Player1", WHITE);
            player2 = new ComputerPlayer("Player2", BLACK);
            Settings settings = new Settings(player1, player2);
            gameObject.setSettings(settings);
            gameObject.newGame();
            //The type of the game is set as LOCAL mode
            gameObject.getSettings().setGameType(GameTypes.LOCAL);
            gameObject.getSettings().getPlayerWhite().setType(PlayerType.COMPUTER);
            gameObject.getSettings().getPlayerBlack().setType(PlayerType.COMPUTER);

            //AI Level1 is selected
            AI ai = new Level1();
            gameObject.setAi(ai);
            return gameObject;

        } else{
            player1 = new HumanPlayer("Player1", BLACK);
            player2 = new ComputerPlayer("Player2", WHITE);
            Settings settings = new Settings(player1, player2);
            gameObject.setSettings(settings);
            gameObject.newGame();
            //The type of the game is set as LOCAL mode
            gameObject.getSettings().setGameType(GameTypes.LOCAL);
            gameObject.getSettings().getPlayerWhite().setType(PlayerType.COMPUTER);
            gameObject.getSettings().getPlayerBlack().setType(PlayerType.COMPUTER);

            //AI Level1 is selected
            AI ai = new Level1();
            gameObject.setAi(ai);
            return gameObject;
        }

    }

    //Function to obtain the first move by Computer when user has chosen to play BLACK
    public ArrayList<Integer> getFirstComputerMove(Game gameObject){
        ArrayList<Integer> listOfSquares = new ArrayList<Integer>();
        if (canDoComputerMove(gameObject))
        {
            //Obtain the co-ordinates of the start and end move done by the computer
            listOfSquares= doComputerMove(gameObject);
            //Maintain the state of the game
            gameObject.updateFenStateText();
        }
        return listOfSquares;
    }

    //Function the quit the game
    public static void quit(Game gameObject){
        gameObject.endGame("Quitting the game");
    }




}
