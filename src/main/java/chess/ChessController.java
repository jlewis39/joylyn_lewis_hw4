//ChessController class handles all incoming HTTP requests from the user and returns an appropriate response
package chess;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.art.lach.mateusz.javaopenchess.core.Game;

import java.util.ArrayList;

//@RestController annotation indicates that this class is a Controller
@RestController

public class ChessController {

    //For a GET or POST request on the endpoint <url>/chess, a welcome message is displayed
    @RequestMapping(value = "/chess", produces = "application/json")
    public Response index(){
        Response response = new Response("Welcome to a game of Chess!");
        return response;
    }

    //For the POST requestm, the user can create a new game with the parameters gameId and firstMove which indicates whether user wants to play BLACK or WHITE
    @PostMapping(value = "/chess/game", produces = "application/json")
    public ResponseEntity<Response> createGame(@RequestParam("gameId") String gameId, @RequestParam("firstMove") boolean firstMove) {
        if (JChessEngine.getGames().containsKey(gameId)) {
            //User is notified if he tries to create a game with a gameId that already exists during the period when the service is run
            return ResponseEntity.status(HttpStatus.FOUND).body(new Response("Game Id already exists! Please create game with different Game Id."));
        }
        //The newGame function is invoked and the hashmap of gameId with game object is saved
        JChessWrapper jChessWrapper = new JChessWrapper();
        Game game = jChessWrapper.newGame(firstMove);
        JChessEngine.getGames().put(gameId, game);

        //Appropriate message is returned to user based on whether he selected WHITE or BLACK
        if(firstMove){
            return ResponseEntity.status(HttpStatus.CREATED).body(new Response("You've chosen to play White! Use endpoint <url>/chess/game/{gameId}/move to play first move"));
        } else{
            return ResponseEntity.status(HttpStatus.CREATED).body(new Response("You've chosen to play Black! Use endpoint <url>/chess/game/{gameId}/getFirstMove to get first move from Computer"));
        }

    }

    //GET request can be used to check whether a particular GameId exists or not during the time when the service is run
    @GetMapping(value = "/chess/game/{gameId}", produces = "application/json")
    @ResponseBody
    public Response checkGameId(@PathVariable("gameId") String gameId){
        //Check is made against the Hashmap which stores the gameId and game object as key, value pair
        if (JChessEngine.getGames().containsKey(gameId)) {
            return new Response("Game Id found");}
        else{
            return new Response("Game id not found");
            }
    }

    //GET request for obtaining the first move from the Computer when the user has selected to play BLACK
    @GetMapping(value = "chess/game/{gameId}/getFirstMove", produces = "application/json")
    public ResponseEntity<Response> firstMoveByComputer(@PathVariable("gameId") String gameId){
        //Check is made to see if the gameId is valid or not
        if (!JChessEngine.getGames().containsKey(gameId)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        //For a valid gameId, the move from the computer is obtained
        ArrayList<Integer> listOfSquares = new ArrayList<Integer>();
        Game game = JChessEngine.getGames().get(gameId);
        JChessWrapper jChessWrapper = new JChessWrapper();
        listOfSquares = jChessWrapper.getFirstComputerMove(game);

        //If no computer move is obtained first, then an indicator of BAD_REQUEST is returned
        if(listOfSquares.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        //For a valid first move by computer, the response is returned to user
        return ResponseEntity.status(HttpStatus.OK).body(new Response("Computer Move", gameId, listOfSquares));
    }

    //POST request to play a move is sent by the user
    @PostMapping(value = "chess/game/{gameId}/move", produces = "application/json")
    public ResponseEntity<Response> movePiece(@PathVariable("gameId") String gameId, @RequestParam("fromX") int fromX,@RequestParam("fromY") int fromY, @RequestParam("toX") int toX, @RequestParam("toY") int toY ){
        //Check to see if the gameId is valid
        if (!JChessEngine.getGames().containsKey(gameId)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        //If move is valid, the move is actually made and computer's move is obtained in response in listOfSquare ArrayList
        ArrayList<Integer> listOfSquares = new ArrayList<Integer>();
        Game game = JChessEngine.getGames().get(gameId);
        JChessWrapper jChessWrapper = new JChessWrapper();
        listOfSquares = jChessWrapper.move(fromX, fromY, toX, toY, game);

        //if computer's move is empty, then an indicator of BAD_REQUEST is returned to the user
        if(listOfSquares.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        //for a valid move, a valid computer move is returned to the user
        return ResponseEntity.status(HttpStatus.OK).body(new Response("Computer Move", gameId, listOfSquares));
    }

    //POST request to quit the game where the user can leave the game at any time
    @PostMapping(value = "/chess/game/quit", produces = "application/json")
    public Response quitGame(@RequestParam("gameId") String gameId){
        //Check to see if the gameId is valid
        if (!JChessEngine.getGames().containsKey(gameId)) {
            return new Response("Game Id not found");
        }
        //Game object is removed from the HashMap
        Game game = JChessEngine.getGames().get(gameId);
        JChessEngine.getGames().remove(gameId);
        return new Response("Quitting the game");
    }



}
