//Enum KingState lists the possible values of king state which is useful in determining whether King is safe and able to play moves
package chess;

public enum KingState {

        FINE,

        CHECKMATED,

        STEALMATED

}
