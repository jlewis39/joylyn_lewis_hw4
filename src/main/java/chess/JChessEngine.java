//JChessEngine class maintains a hashmap where the key is the gameId and value is the game object. This class is used to keep track of the different games that
//are played when the Spring Boot service is run
package chess;

import pl.art.lach.mateusz.javaopenchess.core.Game;
import java.util.HashMap;
import java.util.Map;


public class JChessEngine extends JChessWrapper {
    private static Map<String, Game> games = new HashMap<>();
    //Returns the game object based on the gameId. This helps in retrieving the state of the game for each game that is played when the service is run
    public static Map<String, Game> getGames() {
        return games;
    }

    //Function which save the game Object for the gameId.
    public static void setGames(Map<String, Game> games) {
        JChessEngine.games = games;
    }

}
