//Unit Tests written to test the Web Service REST API calls
package chess;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import pl.art.lach.mateusz.javaopenchess.core.Game;
import static org.junit.Assert.assertEquals;

//@SpringBootTest is used to load the application context
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

public class ChessControllerTest {
    //Function create a random game with gameId and firstMove as true i.e. user plays WHITE
    private Game createRandomGame(String gameId){
        JChessWrapper jChessWrapper = new JChessWrapper();
        Game game = jChessWrapper.newGame(true);
        JChessEngine.getGames().put(gameId, game);
        return game;
    }

    @LocalServerPort
    private int port;

    //port is used here since it is seen that RestAssured uses random ports while running the tests
    @Before
    public void setUp() throws Exception {
        RestAssured.port = port;
    }

    //Test 1: Check that the GET request for welcome message works properly
    @Test
    public void checkIndexTest() throws Exception{
        io.restassured.response.Response response = RestAssured.get("/chess");
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

    //Test 2: Check that a POST request for a new game works properly
    @Test
    public void createRandomGameTest() throws Exception{
        String gameId = "1";
        Game game = createRandomGame(gameId);
        io.restassured.response.Response response = RestAssured.post("/chess/game?gameId=1&firstMove=true");
        assertEquals(HttpStatus.FOUND.value(), response.getStatusCode());
    }

    //Test 3: Check that a GET request to check whether game exists or not works properly
    @Test
    public void checkGameNotExists() throws Exception{
        io.restassured.response.Response response = RestAssured.get("/chess/game/2");
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

    //Test 4: Check that a GET request computer's first move for a game that doesn't exist results in NOT_FOUND status
    @Test
    public void checkFirstMoveByComputerForInvalidGameId() throws Exception{
        io.restassured.response.Response response = RestAssured.get("/chess/game/10/getFirstMove");
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    //Test 5: Check that a POST request to quit a game works properly
    @Test
    public void quitTest() throws Exception{
        io.restassured.response.Response response = RestAssured.post("/chess/game/quit?gameId=1");
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }
}













