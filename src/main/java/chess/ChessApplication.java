//ChessApplication class is treated as the 'Main Class' from which the Java Application will run
package chess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//Adding the @SpringBootApplication annotation to the class to make this a Spring Boot Application
@SpringBootApplication
public class ChessApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChessApplication.class, args);
    }

}
